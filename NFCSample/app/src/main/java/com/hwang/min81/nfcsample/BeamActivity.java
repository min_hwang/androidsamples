package com.hwang.min81.nfcsample;

import android.content.Context;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.charset.Charset;

public class BeamActivity extends AppCompatActivity implements NfcAdapter.CreateNdefMessageCallback {
    private NfcAdapter mNfcAdapter;
    private TextView mTextView;
    private TextView mTextMsg;
    private Boolean mSendMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beam);
        mTextView = (TextView)findViewById(R.id.textView);
        mTextMsg = (TextView)findViewById(R.id.message);
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter == null) {
            Toast.makeText(this, "NFC is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        mNfcAdapter.setNdefPushMessageCallback(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_beam, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void beam(View view) {
        switch (view.getId()) {
            case R.id.buttonSendMsg:
                mSendMsg = true;
                mTextView.setText("Android will send a message.");
                break;
            case R.id.buttonSendPhone:
                mSendMsg = false;
                mTextView.setText("Android will send your phone number.");
                break;
            default:
                break;
        }
    }

    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        NdefMessage msg;

        if (mSendMsg) {
            byte[] langBytes = mTextMsg.getTextLocale().getLanguage().getBytes(Charset.forName("US-ASCII"));
            Charset utfEncoding = Charset.forName("UTF-8");
            byte[] textBytes = mTextMsg.getText().toString().getBytes(utfEncoding);
            int utfBit = 0;
            char status = (char)(utfBit + langBytes.length + textBytes.length);
            byte[] data = new byte[1 + langBytes.length + textBytes.length];
            data[0] = (byte)status;
            System.arraycopy(langBytes, 0, data, 1, langBytes.length);
            System.arraycopy(textBytes, 0, data, 1 + langBytes.length, textBytes.length);
            NdefRecord record = new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], data);
            msg = new NdefMessage(
                    new NdefRecord[] { record, }
            );
        }
        else {
            TelephonyManager tMgr = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
            String myNumber = tMgr.getLine1Number();

            msg = new NdefMessage(
                    new NdefRecord[] { NdefRecord.createUri("tel:" + myNumber) }
            );
        }
        return msg;
    }
}
