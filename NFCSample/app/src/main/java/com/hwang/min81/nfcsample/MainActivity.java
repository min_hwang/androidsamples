package com.hwang.min81.nfcsample;

import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Parcelable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {
    private Intent ndefIntent = null;
    private Intent tagIntent = null;
    private Intent techIntent = null;
    private Intent beamIntent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void startActivity(View view) {
        Intent intent = null;

        switch (view.getId()) {
            case R.id.buttonNdef:
                if(this.ndefIntent == null) { this.ndefIntent = new Intent(this, NdefDiscoveredActivity.class); }
                intent = this.ndefIntent;
                break;
            case R.id.buttonTech:
                if(this.techIntent == null) { this.techIntent = new Intent(this, TechDiscoveredActivity.class); }
                intent = this.techIntent;
                break;
            case R.id.buttonTag:
                if(this.tagIntent == null) { this.tagIntent = new Intent(this, TagDiscoveredActivity.class); }
                intent = this.tagIntent;
                break;
            case R.id.buttonBeam:
                if(this.beamIntent == null) { this.beamIntent = new Intent(this, BeamActivity.class); }
                intent = this.beamIntent;
                break;
            default:
                break;
        }

        if(intent != null) {
            startActivity(intent);
        }
    }
}
