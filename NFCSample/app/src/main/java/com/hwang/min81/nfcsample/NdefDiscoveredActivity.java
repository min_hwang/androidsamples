package com.hwang.min81.nfcsample;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.charset.Charset;
import java.util.Arrays;

public class NdefDiscoveredActivity extends AppCompatActivity {
    private NfcAdapter mNfcAdapter;
    private PendingIntent mPendingIntent;
    private IntentFilter[] mIntentFilters;

    final String[] URI_PREFIX = new String[] {
    /* 0x00 */ "",
    /* 0x01 */ "http://www.",
    /* 0x02 */ "https://www.",
    /* 0x03 */ "http://",
    /* 0x04 */ "https://",
    /* 0x05 */ "tel:",
    /* 0x06 */ "mailto:",
    /* 0x07 */ "ftp://anonymous:anonymous@",
    /* 0x08 */ "ftp://ftp.",
    /* 0x09 */ "ftps://",
    /* 0x0A */ "sftp://",
    /* 0x0B */ "smb://",
    /* 0x0C */ "nfs://",
    /* 0x0D */ "ftp://",
    /* 0x0E */ "dav://",
    /* 0x0F */ "news:",
    /* 0x10 */ "telnet://",
    /* 0x11 */ "imap:",
    /* 0x12 */ "rtsp://",
    /* 0x13 */ "urn:",
    /* 0x14 */ "pop:",
    /* 0x15 */ "sip:",
    /* 0x16 */ "sips:",
    /* 0x17 */ "tftp:",
    /* 0x18 */ "btspp://",
    /* 0x19 */ "btl2cap://",
    /* 0x1A */ "btgoep://",
    /* 0x1B */ "tcpobex://",
    /* 0x1C */ "irdaobex://",
    /* 0x1D */ "file://",
    /* 0x1E */ "urn:epc:id:",
    /* 0x1F */ "urn:epc:tag:",
    /* 0x20 */ "urn:epc:pat:",
    /* 0x21 */ "urn:epc:raw:",
    /* 0x22 */ "urn:epc:",
    /* 0x23 */ "urn:nfc:"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ndef_discovered);

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter == null) {
            Toast.makeText(this, "NFC is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        IntentFilter intentFilterText = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        try {
            intentFilterText.addDataType("text/plain");
        } catch (IntentFilter.MalformedMimeTypeException e) {
            e.printStackTrace();
        }

        IntentFilter intentFilterUri = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        intentFilterUri.addDataScheme("tel:");

        mIntentFilters = new IntentFilter[] { intentFilterText, intentFilterUri };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ndef_discovered, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        process();
        mNfcAdapter.enableForegroundDispatch(this, mPendingIntent, mIntentFilters, null);
    }

    @Override
    public void onPause() {
        super.onPause();
        mNfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        process();
    }

    private void process() {
        Intent intent = getIntent();
        TextView tx = (TextView)findViewById(R.id.textView);

        if(NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (rawMsgs != null && rawMsgs.length > 0) {
                NdefRecord[] records = ((NdefMessage)rawMsgs[0]).getRecords();
                for (int i = 0; i < records.length; i++) {
                    if(records[i].getTnf() == NdefRecord.TNF_WELL_KNOWN) {
                        if(Arrays.equals(records[i].getType(), NdefRecord.RTD_TEXT)) {
                            byte offset = records[i].getPayload()[0];
                            //tx.setText(new String(records[i].getPayload(), offset + 1, records[i].getPayload().length - offset - 1));
                            tx.setText(new String(records[i].getPayload()));
                        }
                        else if(Arrays.equals(records[i].getType(), NdefRecord.RTD_URI)) {
                            byte[] payload = records[i].getPayload();
                            int prefixCode = payload[0] & 0x0FF;
                            if(prefixCode >= URI_PREFIX.length) prefixCode = 0;

                            if(prefixCode == 0x05) {
                                String uri = URI_PREFIX[prefixCode] + new String(payload, 1, payload.length - 1, Charset.forName("UTF-8"));
                                Intent dialIntent = new Intent(Intent.ACTION_DIAL, Uri.parse(uri));
                                startActivity(dialIntent);
                            }
                            else { tx.setText("Not supported URI prefix"); }
                        }
                        else { tx.setText("Not supported type."); }
                    }
                    else { tx.setText("Not supported TNF."); }
                }
            }
        }
    }
}
